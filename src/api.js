import { prepareData } from './utils/utils';

const APP_ID = 'ded9254ef1643f29235b20285f0d2046';
const WEATHER_URL = 'https://api.openweathermap.org/data/2.5/forecast?';
const MODE = 'json';
const DUBLIN_ID = '2964574';
const IMAGE_PATH = 'https://openweathermap.org/img/w/'
const TIME_SPAN = ["0:00", "3:00", "6:00", "9:00", "12:00", "15:00", "18:00", "21:00"];

export const getWeather = (options) => {
    const {units, lang } = options;
    const CONSUTRCTED_WEATHER_URL = `${WEATHER_URL}id=${DUBLIN_ID}&lang=${lang}&mode=${MODE}&units=${units}&appid=${APP_ID}`;

    return fetch(CONSUTRCTED_WEATHER_URL)
        .then(res => res.json())
        .then(res => res.cod === "200" ? prepareData(res) : { ...res, error: true })
}

export const getWeatherImageURL = (icon) => {
    return `${IMAGE_PATH}${icon}.png`
}

export const getTimeSpans = () => TIME_SPAN;