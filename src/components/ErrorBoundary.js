
import React, { Component } from 'react';

export class ErrorBoundary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hasError: false
        };
    }

    componentDidCatch(){
        this.setState(state => ({...state, hasError: true}));
    }

    render() {
        if (this.state.hasError) {
            return <div> Sorry, something went wrong. Please contact me https://github.com/alexandrzavalii for support.</div>
        } else {
            return this.props.children;
        }
    }
}