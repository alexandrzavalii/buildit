import React from 'react';
import PropTypes from 'prop-types';
import "./RefreshButton.css";
  
export const RefreshButton = ({ handleClick }) => (
    <button className="RefreshButton" onClick={handleClick}>
        Refresh
    </button>
);

RefreshButton.propTypes = {
    handleClick: PropTypes.func
};
