import React from 'react';
import PropTypes from 'prop-types';

import "./WeatherForecast.css";
import { getWeatherImageURL, getTimeSpans } from '../../api';
import { getMonthName, getDayOfWeek } from '../../utils/dateFormat';

export const WeatherForecast = ({ weather }) => {
    const timeSpans = getTimeSpans();
    return (
        <div className="WeatherForecast">
            <div className="single labels">
                <div className="period">Time</div>
                {timeSpans && timeSpans.map(item =>
                    <div key={item} className="period">{item}</div>)}
            </div>
            {Object.keys(weather).map(key =>
                <SingleDay day={weather[key]} key={key} date={key} />)}
        </div>
    )
}

const SingleDay = ({ day, date }) => {
    const monthName = getMonthName(day[Object.keys(day)[0]].dt);
    const dayOfTheWeek = getDayOfWeek(day[Object.keys(day)[0]].dt);
    const today = new Date().getDate();
    const isToday = today.toString() === date;
    const className = isToday ? "single today" : "single";
    return (
        <div className={className}>
            <div className="date">
                <span>{monthName} {date}</span>
                <span>{dayOfTheWeek}</span>
            </div>
            {Object.keys(day).map(key =>
                <SinglePeriod
                    key={key}
                    day={day}
                    period={key}
                    isToday={isToday} />)}
        </div>
    )
}

SingleDay.propTypes = {
    day: PropTypes.object,
    date: PropTypes.string
}

const SinglePeriod = ({ isToday, day, period }) => {
    let periodTemp = day[period];
    let divStyle;
    if (isToday && day[period].dt === day[Object.keys(day)[0]].dt) {
        const gridRow = 11 - Object.keys(day).length;
        divStyle = {
            gridRow: "2/" + gridRow,
            alignSelf: "flex-end"
        }
    }

    return (
        <div style={divStyle} className="period">
            <div className="time"> {period}:00</div>
            <div>{periodTemp.main.temp} &#8451;</div>
            <WeatherIcon icon={periodTemp.weather[0].icon} />
            <div>{periodTemp.weather[0].description}</div>
        </div>);
}

SinglePeriod.propTypes = {
    day: PropTypes.object,
    isToday: PropTypes.bool,
    period: PropTypes.string
}

const WeatherIcon = ({ icon }) => {
    const imgURL = getWeatherImageURL(icon);
    return <img src={imgURL} with="40px" height="40px" alt="weather" />
}

WeatherIcon.propTypes = {
    icon: PropTypes.string
}