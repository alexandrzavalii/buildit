import React from 'react';
import PropTypes from 'prop-types';

export const ErrorMessage = ({ message }) => {
    return (
        <h3>
            Error: {message}
        </h3>
    );
}

ErrorMessage.propTypes = {
    message: PropTypes.string
};
