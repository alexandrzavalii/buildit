import React, { Component } from 'react';
import './App.css';
import { WeatherForecast } from '../components/WeatherForecast/WeatherForecast';
import { RefreshButton } from '../components/RefreshButton/RefreshButton';
import { ErrorMessage } from '../components/ErrorMessage';
import { Loading } from '../components/Loading/Loading';
import { getWeather } from '../api';
import { saveToLocalStorage, getFromLocalStorage } from '../utils/utils';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      city: 'Dublin',
      units: 'metric',
      lang: 'en',
      weather: [],
      error: false,
      errorMessage: '',
      loading: true
    };

    this.getData = this.getData.bind(this);
    this.clickedRefresh = this.clickedRefresh.bind(this);
  }

  componentDidMount() {
    this.checkFromLocalStorage();
    this.getData();
  }

  checkFromLocalStorage() {
    const response = getFromLocalStorage();
    if (response) {
      const parsedResponse = JSON.parse(response);
      this.setState({ weather: parsedResponse.list })
    }
  }

  async getData() {
    const { units, lang } = this.state;
    const response = await getWeather({ units, lang });

    if (response.error) {
      this.setState(previousState => ({
        loading: false,
        error: true,
        errorMessage: response.message
      }));
    } else {
      saveToLocalStorage(response);
      this.setState(previousState => ({
        loading: false,
        error: false,
        weather: response.list
      }));
    }
  }

  clickedRefresh() {
    this.setState({ loading: true }, () => this.getData())
  }

  render() {
    return (
      <div className="App">
        <h2>Weather Forecast for {this.state.city}</h2>
        {this.state.loading ? <Loading /> :
          <RefreshButton handleClick={this.clickedRefresh} />
        }
        {this.state.error ? <ErrorMessage message={this.state.errorMessage} /> :
          <WeatherForecast weather={this.state.weather} city={this.state.city} />
        }
      </div>
    );
  }
}

export default App;
