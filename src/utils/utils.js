export const prepareData = (response) => {
    response.list = response.list.reduce((resultedObject, day) => {
        const datetime = new Date(day.dt * 1000);
        const date = datetime.getDate();
        const hours = datetime.getHours();
        if (!resultedObject[date]) {
            resultedObject[date] = {};
        }
        resultedObject[date][hours] = day;
        return resultedObject;
    }, {});

    return response;
}

export const saveToLocalStorage = (response) => {
    localStorage.setItem("response", JSON.stringify(response));
}

export const getFromLocalStorage = () => localStorage.getItem("response");