
import { getMonthName, getDayOfWeek } from "./dateFormat";

test('Convert Unix Timestamp to 3 letters code of the month', () => {
    const UNIX_TIMESTAMP = 1516536000;
    const MONTH_NAME = "Jan";
    const mothName = getMonthName(UNIX_TIMESTAMP);

    expect(mothName).toBe(MONTH_NAME);
});

test('Convert Unix Timestamp to 3 letters code of the day of the week', () => {
    const UNIX_TIMESTAMP = 1516536000;
    const DAY_OF_WEEK = "Sun";
    const dayOfWeek = getDayOfWeek(UNIX_TIMESTAMP);

    expect(dayOfWeek).toBe(DAY_OF_WEEK);
});
