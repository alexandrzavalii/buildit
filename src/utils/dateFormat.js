export const getMonthName = (timestamp) => {
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ];

    const d = new Date(timestamp * 1000);
    return monthNames[d.getMonth()];
}
export const getDayOfWeek = (timestamp) => {
    const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    const d = new Date(timestamp * 1000);
    return days[d.getDay()];
}
