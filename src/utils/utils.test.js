
import { prepareData } from "./utils";

const MOCK_RESPONSE = `{
    "list":[
        {"dt":1516536000},
        {"dt":1516546800}
    ]}`;

const EXPECTED_PARSED_DATA = {
    list: {
        21: {
            12: { dt: 1516536000 },
            15: { dt: 1516546800 }
        }
    }
};

test('Preparedata function returns correct format', () => {
    const response = JSON.parse(MOCK_RESPONSE);
    const parsedResponse = prepareData(response)

    expect(parsedResponse.toString()).toBe(EXPECTED_PARSED_DATA.toString());
});
